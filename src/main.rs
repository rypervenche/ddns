use ansi_term::Color::{Green, Yellow};
use anyhow::Result;
use clap::Parser;
use ddns::*;

fn main() -> Result<()> {
    root_user_check()?;
    let opts = Opts::parse();
    let subdomain = opts.subdomain;
    let domain = sub_to_root(&subdomain);
    let mut zone = get_zone(&domain)?;
    let record = get_record_info(&zone, &subdomain)?;

    if opts.ip != record.ip {
        backup_zone_file(&zone.path)?;
        update_serial(&mut zone)?;
        replace_record_ip(&mut zone, &record, &opts.ip)?;
        write_zone_file(&zone)?;
        restart_bind()?;
        println!("{}", Yellow.bold().paint("IP address changed."));
    } else {
        println!("{}", Green.bold().paint("IP address unchanged."));
    }

    Ok(())
}
