//! Update DNS server with current WAN IP

#![warn(missing_docs)]

use anyhow::Result;
use chrono::Local;
use clap::Parser;
use lazy_static::lazy_static;
use regex::{Captures, Regex};
use std::fs;
use std::io::{self, Write};
use std::net::Ipv4Addr;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::str::FromStr;
use thiserror::Error;
use users::get_current_uid;

const ZONE_DIR: &str = "/var/bind";

/// Custom errors
#[derive(Error, Debug)]
pub enum CustomError {
    /// Zone file not found
    #[error("Zone file not found")]
    ZoneNotFound,
    /// Record not found
    #[error("Record not found")]
    RecordNotFound,
    /// Not running as root user
    #[error("Not running as root user")]
    NotRootUser,
    /// Backup cp failed
    #[error("Backup cp failed")]
    BackupFailure,
}

/// CLI options
#[derive(Parser, Debug)]
#[clap(version, about = "Update dynamic DNS for home use")]
pub struct Opts {
    /// Subdomain to update
    pub subdomain: String,

    /// IP address to update DNS with
    #[clap(value_name = "IP")]
    pub ip: Ipv4Addr,
}

/// Zone record class
#[derive(Debug, PartialEq, Eq)]
pub enum Class {
    /// Internet
    IN,
}

impl FromStr for Class {
    type Err = ();

    fn from_str(input: &str) -> Result<Class, Self::Err> {
        match input {
            "IN" => Ok(Class::IN),
            _ => Err(()),
        }
    }
}

/// Zone record type
#[derive(Debug, PartialEq, Eq)]
pub enum Type {
    /// IPv4 address
    A,
    /// IPv6 address
    AAAA,
    /// Canonical name
    CNAME,
    /// Start of Authority
    SOA,
    /// Name server
    NS,
    /// Text
    TXT,
    /// Mail server
    MX,
}

impl FromStr for Type {
    type Err = ();

    fn from_str(input: &str) -> Result<Type, Self::Err> {
        match input {
            "A" => Ok(Type::A),
            "AAAA" => Ok(Type::AAAA),
            "CNAME" => Ok(Type::CNAME),
            "SOA" => Ok(Type::SOA),
            "NS" => Ok(Type::NS),
            "TXT" => Ok(Type::TXT),
            "MX" => Ok(Type::MX),
            _ => Err(()),
        }
    }
}

/// Record from a DNS zone
#[derive(Debug, PartialEq, Eq)]
pub struct Record {
    /// Root domain or subdomain
    pub domain: String,
    /// Zone class
    pub class: Class,
    /// Zone type
    pub r#type: Type,
    /// IPv4 address
    pub ip: Ipv4Addr,
}

/// DNS zone file
#[derive(Debug, PartialEq, Eq)]
pub struct Zone {
    /// Entire zone file
    pub content: String,
    /// Path to zone file
    pub path: PathBuf,
}

lazy_static! {
    static ref RE_SERIAL: Regex = Regex::new(
        r"(?xm)
              ^
              (?P<before>\s+)
              (?P<serialdate>\d{8})
              (?P<serialsuffix>\d{2})
              (?P<after>[^S]+Serial\s*)
              $
        "
    )
    .unwrap();
}

/// Find full path to DNS zone file
fn get_zone_path(domain: &str) -> Result<PathBuf> {
    let zone_file = Path::new(ZONE_DIR).join(format!("{}{}", &domain, ".zone"));

    if !zone_file.is_file() {
        return Err(CustomError::ZoneNotFound.into());
    }
    Ok(zone_file)
}

/// Create Zone instance
pub fn get_zone(domain: &str) -> Result<Zone> {
    let zone_path = get_zone_path(domain)?;
    let string = std::fs::read_to_string(&zone_path)?;
    Ok(Zone {
        content: string,
        path: zone_path,
    })
}

/// Back up zone file if it will be updated
pub fn backup_zone_file(src: &Path) -> Result<()> {
    let now = Local::now().format("%Y-%m-%dT%H:%M:%S%:z").to_string();

    let dest = src.with_extension(format!("bak-{}", now));
    let dest = dest.to_string_lossy();
    let src = src.to_string_lossy();

    let output = Command::new("cp").args(["-a", &src, &dest]).output()?;
    io::stdout().write_all(&output.stdout)?;
    io::stderr().write_all(&output.stderr)?;
    match output.status.success() {
        true => Ok(()),
        false => Err(CustomError::BackupFailure.into()),
    }
}

/// Error and quit if not running as root user
pub fn root_user_check() -> Result<()> {
    if get_current_uid() != 0 {
        return Err(CustomError::NotRootUser.into());
    }
    Ok(())
}

/// Create Record instance
pub fn get_record_info(zone: &Zone, subdomain: &str) -> Result<Record> {
    let mut iter = subdomain.split('.');
    let sub = iter.next().unwrap();
    let domain = iter.collect::<Vec<&str>>().join(".");
    let re_record: Regex = Regex::new(&format!(
        "(?xm)
            ^
            (?P<host>{}(?:\\.{})?)
            \\s+
            (?P<class>\\w+)
            \\s+
            (?P<type>\\w+)
            \\s+
            (?P<ip>[0-9.]+?)
            $
        ",
        sub, domain
    ))
    .unwrap();

    let caps = re_record.captures(&zone.content).unwrap();

    Ok(Record {
        domain: caps.name("host").unwrap().as_str().to_string(),
        class: Class::from_str(caps.name("class").unwrap().as_str()).unwrap(),
        r#type: Type::from_str(caps.name("type").unwrap().as_str()).unwrap(),
        ip: caps.name("ip").unwrap().as_str().parse()?,
    })
}

/// Replace IP address for a given record
pub fn replace_record_ip(zone: &mut Zone, record: &Record, ip: &Ipv4Addr) -> Result<()> {
    let re_domain: Regex = Regex::new(&format!(
        "(?xm)
            ^
            (?P<host>{})
            (?P<middle>\\s+IN\\s+A\\s+)
            (?P<ip>[0-9.]+?)
            $
        ",
        record.domain
    ))
    .unwrap();

    zone.content = re_domain
        .replace(&zone.content, format!("${{host}}${{middle}}{}", ip))
        .to_string();
    Ok(())
}

/// Update serial in zone file's content
pub fn update_serial(zone: &mut Zone) -> Result<()> {
    let today = Local::now().naive_local().format("%Y%m%d").to_string();

    let output = RE_SERIAL
        .replace(&zone.content, |caps: &Captures| {
            let serial_suffix = if &caps["serialdate"] >= &today {
                let ending = &caps["serialsuffix"].to_string();
                format!("{:02}", ending.parse::<u8>().unwrap() + 1)
            } else {
                "00".to_string()
            };
            let serial = if &caps["serialdate"] < &today {
                format!("{}{}", today, serial_suffix)
            } else {
                format!("{}{}", &caps["serialdate"], serial_suffix)
            };
            format!("{}{}{}", &caps["before"], serial, &caps["after"])
        })
        .to_string();

    zone.content = output;

    Ok(())
}

/// Write string to zone file
pub fn write_zone_file(zone: &Zone) -> Result<()> {
    Ok(fs::write(&zone.path, &zone.content)?)
}

/// Reload named/bind service
pub fn restart_bind() -> Result<()> {
    let output = Command::new("systemctl")
        .arg("restart")
        .arg("bind")
        .output()?;
    io::stdout().write_all(&output.stdout)?;
    io::stderr().write_all(&output.stderr)?;
    Ok(())
}

/// Extract root domain from FQDN
pub fn sub_to_root(subdomain: &str) -> String {
    let mut vec: Vec<&str> = subdomain.split('.').collect();
    vec.remove(0);
    vec.join(".")
}

#[cfg(test)]
mod tests {
    use super::*;

    const IP_INPUT1: &str = "$TTL 3H\n\
        home            IN      A       123.123.123.123\n\
        ns1        IN    A    111.111.111.111";

    const IP_INPUT2: &str = "home            IN      A       123.123.123.123";
    const IP_GOOD1: &str = "$TTL 3H\n\
        home            IN      A       127.0.0.1\n\
        ns1        IN    A    111.111.111.111";

    const IP_GOOD2: &str = "home            IN      A       127.0.0.1";

    const SERIAL_IN_GOOD1: &str = "$TTL 3H
                                              2021102000 ; Serial
                                      28800      ; Refresh";
    const SERIAL_IN_GOOD2: &str = "  2021102000 ; Serial";
    const SERIAL_IN_GOOD3: &str = "  2021102000 ; Serial   ";
    const SERIAL_IN_BAD1: &str = "2021102000 ; Serial";
    const SERIAL_IN_BAD2: &str = "   202110200 ; Serial";
    const SERIAL_IN_BAD3: &str = "   2021102000Serial";

    #[test]
    fn replace_record_ip_test() {
        let address = Ipv4Addr::new(127, 0, 0, 1);
        let mut zone_input1 = Zone {
            content: IP_INPUT1.to_string(),
            path: PathBuf::from("/tmp/test.com.zone"),
        };

        let mut zone_input2 = Zone {
            content: IP_INPUT2.to_string(),
            path: PathBuf::from("/tmp/test.com.zone"),
        };

        let zone_good1 = Zone {
            content: IP_GOOD1.to_string(),
            path: PathBuf::from("/tmp/test.com.zone"),
        };

        let zone_good2 = Zone {
            content: IP_GOOD2.to_string(),
            path: PathBuf::from("/tmp/test.com.zone"),
        };

        let record = Record {
            domain: "home".to_string(),
            class: Class::IN,
            r#type: Type::A,
            ip: Ipv4Addr::new(127, 0, 0, 1),
        };

        replace_record_ip(&mut zone_input1, &record, &address).unwrap();
        replace_record_ip(&mut zone_input2, &record, &address).unwrap();
        assert_eq!(zone_good1, zone_input1);
        assert_eq!(zone_good2, zone_input2);
    }

    #[test]
    fn update_serial_test() {
        let date = Local::now().naive_local().format("%Y%m%d00").to_string();

        let serial_out_good1 = format!(
            "$TTL 3H
                                              {} ; Serial
                                      28800      ; Refresh",
            date
        );

        let mut zone_serial_in_good1 = Zone {
            content: SERIAL_IN_GOOD1.to_string(),
            path: PathBuf::from("/tmp/test.com.zone"),
        };

        let zone_serial_out_good1 = Zone {
            content: serial_out_good1,
            path: PathBuf::from("/tmp/test.com.zone"),
        };

        update_serial(&mut zone_serial_in_good1).unwrap();
        assert_eq!(zone_serial_out_good1, zone_serial_in_good1);
    }

    #[test]
    fn serial_regex() {
        assert!(RE_SERIAL.is_match(SERIAL_IN_GOOD1));
        assert!(RE_SERIAL.is_match(SERIAL_IN_GOOD2));
        assert!(RE_SERIAL.is_match(SERIAL_IN_GOOD3));
        assert!(!RE_SERIAL.is_match(SERIAL_IN_BAD1));
        assert!(!RE_SERIAL.is_match(SERIAL_IN_BAD2));
        assert!(!RE_SERIAL.is_match(SERIAL_IN_BAD3));
    }
}
